var TAG = '[MainController]';
angular.module('starter.controller').controller('MainController', 
	function ($scope, $state, $ionicLoading, $q) {
        $scope.googleLogin = function() {
            console.log(TAG,'Google Login Button Click');
        }

        /**
         * Executada quando o botão de login do facebook é clicado.
         */
        $scope.facebookLogin = function() {
            console.log(TAG,'Facebook Login Button Click');
            facebookConnectPlugin.getLoginStatus(
                function(success){
                    if(success.status === 'connected'){
                        // O usuario esta conectado e autenticou o aplicativo.
                        // Como resposta, temos o id do usuario, um token válido de acesso, uma requisição logada
                        // E o tempo em que o token e a requisição expiram.
                        console.log(TAG, 'getLoginStatus: '+success.status);

                        //Agora já podemos trabalhar com o resultado que o facebook retorna
                        getFacebookProfileInfo(success.authResponse)
                        .then(function(profileInfo) {
                            connectProfileToApp(profileInfo, success.authResponse);
                        }, function(fail){
                            console.log(TAG, 'Erro ao pegar as informações do perfil: '+fail);
                        });
                    } else {
                        // Se success.status === 'not_authorized' o usuario esta logado no facebook,
                        // mas não atenticou o aplicativo.
                        // Caso contrário, a pessoa não está logada no facebook,
                        // logo, não é possível ter certeza se ela esta conectada no aplicativo ou não.

                        console.log('getLoginStatus', success.status);

                        $ionicLoading.show({
                            template: 'Logging in...'
                        });

                        // Solicitando informações básicas de login.
                        facebookConnectPlugin.login(['email', 'public_profile'], fbLoginSuccessCallback, fbLoginErrorCallback);
                    }
                }
            );
        }

        /**
         * Apenas pega os dados do perfil do usuário para armazenar no app.
         * No caso, apenas exibe no console.
         */
        var connectProfileToApp = function (profileInfo, authResponse) {
            console.log(TAG, profileInfo);
            console.log(TAG, 'profile image: '+"http://graph.facebook.com/" + authResponse.userID + "/picture?type=large");
            console.log(TAG, 'Usuário logado com sucesso!');
            $ionicLoading.hide();
        }

        /**
         * Função de callback para login via facebook 
         * executado corretamente
         */
        var fbLoginSuccessCallback = function(response) {
            if (!response.authResponse){
                fbLoginErrorCallback(TAG, "Cannot find the authResponse");
                return;
            }

            var authResponse = response.authResponse;

            getFacebookProfileInfo(authResponse)
            .then(function(profileInfo) {
                //Agora já podemos trabalhar com o resultado que o facebook retorna
                getFacebookProfileInfo(authResponse)
                .then(function(profileInfo) {
                    connectProfileToApp(profileInfo, authResponse);
                }, function(fail){
                    console.log(TAG, 'Erro ao pegar as informações do perfil: '+fail);
                });
            });
        };

        var fbLoginErrorCallback = function(error){
            console.log(TAG, 'Facebook Login Error:'+error.getMessage());
            $ionicLoading.hide();
        };

        /**
         * Método para pegar o perfil do usuario da API do Facebook
         */
        var getFacebookProfileInfo = function (authResponse) {
            var info = $q.defer();

            facebookConnectPlugin.api('/me?fields=email,name&access_token=' + authResponse.accessToken, null,
                function (response) {
                    console.log(TAG, response);
                    info.resolve(response);
                },
                function (response) {
                    console.log(TAG, response);
                    info.reject(response);
                }
            );
            return info.promise;
        };

    }
)